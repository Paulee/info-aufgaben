liste1 = [1, 234, 4, 5, 6, 54, 5, 457645, 645]
liste2 = [0, 32, 323, 7]

liste1.extend(liste2)

print(liste1)

# --------------------------------------

while 5 in liste1:
    liste1.remove(5)

print(liste1)

'''
    für krasse:
    print(list(filter(lambda a : a != 5, liste1)))
'''

# --------------------------------------

index = int(input("An welcher Stelle?"))
while index < 0 or index > len(liste1) - 1:
    print("Der gewählte Index ist zu Groß oder zu klein")
    index = int(input("An welcher Stelle?"))

print(liste1[: index])
print(liste1[index:])

# --------------------------------------

listeReverse = [1, 3, 5, 8, 9, 3]

# liste1.reverse()

for i in range(len(listeReverse)):
    listeReverse.insert(i, listeReverse.pop(len(listeReverse) - 1))

print(listeReverse)

# --------------------------------------
# fixen
# Primzahlen

# --------------------------------------

satz = input("Dein Satz?")
wort = input("Dein Wort?")
print(satz.__contains__(wort))

# --------------------------------------

muenzen = []
muenzenA = []
cent = int(input("Wie viele Cent-Stücke?"))
centA = cent

for i in [200, 100, 50, 20, 10, 5, 2, 1]:
    anzahl = cent // i
    cent %= i
    for j in range(anzahl):
        if i > 50:
            muenzen.append(str(i // 100) + "-Münze")
        else:
            muenzen.append(str(i) + "-Cent")

print(muenzen)

print("Omi Advanced:")

for i in [200, 100, 50, 20, 10, 5, 2, 1]:
    anzahl = centA // i
    centA %= i
    if i > 50:
        muenzenA.append(str(anzahl) + " * " + str(i // 100) + "€-Münze")
    else:
        muenzenA.append(str(anzahl) + " * " + str(i) + "-Cent")

print(muenzenA)