alphabet = "abcdefghijklmnopqrstuvwxyz"

eingabe = input("Welchen Satz möchtest du verschlüsseln?\n")
verschiebung = int(input("Wie weit willst du verschieben?\n"))

caesared = ""

for i in eingabe.lower():
    if i not in alphabet:
        caesared += i
    else:
        if alphabet.find(i) + verschiebung <= 25:
            caesared += alphabet[alphabet.find(i) + verschiebung]
        else:
            caesared += alphabet[((alphabet.find(i) + verschiebung) % 26)]

print(caesared)