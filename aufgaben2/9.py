from random import randint as rnd


def lotto():
    numbers = []
    for i in range(6):
        n = rnd(1, 49)
        while numbers.__contains__(n):
            n = rnd(1, 49)

        numbers.append(n)

    print(numbers)


lotto()
