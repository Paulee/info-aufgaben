import random

def rate_die_zahl():
    geheim = random.randint(1, 25)
    while True:
        print("Raten Sie eine Zahl zwischen 1 und 25")
        eingabe = int(input("Ihre Vermutung: "))

        if eingabe == geheim:
            print("Richtig! Das ist die gesuchte Zahl!")
            break
        else:
            if eingabe > geheim:
                print("Ihre Zahl ist zu groß. Nochmal von vorne!")
            else:
                print("Ihre Zahl ist zu klein. Nochmal von vorne!")


rate_die_zahl()