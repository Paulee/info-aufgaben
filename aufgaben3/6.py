def hole_datei(datei_name):
    try:
        d = open(datei_name, 'r')
        text = d.read()
        d.close()
        return text
    except FileNotFoundError:
        return "Ihre angegebene Datein konnte nicht gefunden werden!"


print(hole_datei("test.txt"))
