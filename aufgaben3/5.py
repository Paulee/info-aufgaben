'''
    Aufgaben beziehen sich auf ältere Versionen von Python!
    In Python 3.x wird es so gemacht:
'''


def schreibe_datei(datei_name, zu_schreiben):
    d = open(datei_name, 'w')
    d.write(str(zu_schreiben))
    d.close()


def hole_datei(datei_name):
    d = open(datei_name, 'r')
    text = d.read()
    d.close()
    return text


schreibe_datei("test.txt", "Test")
print(hole_datei("test.txt"))