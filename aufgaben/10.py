a = float(input("a der Gleichung?"))    #Eingabe der Variablen (a,b,c)
b = float(input("b der Gleichung?"))
c = float(input("c der Gleichung?"))

diskriminante = (b * b) - 4 * a * c     #Errechnung der Diskriminante

if diskriminante > 0:                   #If Schleife die prüft, ob die diskriminante kleiner als 0 ist
    print("Es gibt mehrere Lösungen!")

    x1 = (-b + (diskriminante ** 0.5)) / (2 * a)    #Errechnung der x Werte
    x2 = (-b - (diskriminante ** 0.5)) / (2 * a)    #Errechnung der x Werte

    print("x1 =", x1)                               #Ausgabe X1
    print("x2 =", x2)                               #Ausgabe X2
elif diskriminante == 0:                 #Zweite Option der if Schleife die prüft, ob die diskriminante 0 ist
    print("Es gibt eine Lösung!")

    print("x =", -b / (2 * a))            #Errechnung des X Wertes und Ausgabe
else:
    print("Es gibt keine Lösung!")