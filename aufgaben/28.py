e = int(input("Wie viel Euro?"))
c = int(input("Wie viel Cent?"))

print("----Euro----")
for i in [100, 50, 20, 10, 5, 2, 1]:
    print(str(i) + "-Euro:", e // i)
    e %= i
print("----Cent----")
for i in [50, 20, 10, 5, 2, 1]:
    print(str(i) + "-Cent:", c // i)
    c %= i
