personen = []   #leere Liste
stimmen = []    #leere Liste

for n in range(3):
    personen.append(input("Person " + str(n + 1)))              #Input, der die Zahlen zur Liste hinzufügtv
    stimmen.append(int(input("Stimmen für " + personen[n])))    #Input, der die Zahlen zur Liste hinzufügt

alle = sum(stimmen)     #Errechnung der Summe der Stimmen

for n in range(3):      #For Schleife, die 3 mal ausgeführt wird
    print(personen[n], "hat", str(round(stimmen[n] / alle * 100, 1)) + "% der Stimmen")     #Ausgabe der Personen und den zugehörigen Werten