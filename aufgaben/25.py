import random

richtig = 0

for i in range(10):
    x = random.randint(0, 1000)
    y = random.randint(0, 1000)
    print("Aufgabe", str(i + 1) + ":", x, "+", y)
    if (x + y) == int(input("=")):
        richtig += 1

print("Du hattest", str(richtig), "richtig!")