sekunden = int(input("Wie viele Sekunden?"))

tage = sekunden // 86400

sekunden %= 86400
stunden = sekunden // 3600

sekunden %= 3600
minuten = sekunden // 60

sekunden %= 60

print("Tage:", tage, "Stunden:", stunden, "Minuten:", minuten, "Sekunden:", sekunden)
