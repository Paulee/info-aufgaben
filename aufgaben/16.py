import random                       #Import des Moduls "Random"

x = random.randint(-1000, 1000)     #X wird als zufälliger Wert des Bereiches -1000 bis 1000 generiert
y = random.randint(-1000, 1000)     #Y wird als zufälliger Wert des Bereiches -1000 bis 1000 generiert

print("Was kommt da raus?")         #

if int(input(str(x) + " + " + str(y) + " =")) == (x + y):   #Ausgabe von x und y | Prüfung ob der eingegebene wert mit dem tatsächlichen Ergabnis übereinstimmt
    print("Hast du fein gemacht!")
else:
    print("Dumm?")