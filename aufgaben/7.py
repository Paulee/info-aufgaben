a = float(input("Welche Breite?"))      #Funktionsweise siehe Aufgabe 6!!
b = float(input("Welche Höhe?"))
c = float(input("Welche Länge?"))

if a <= 0 or b <= 0 or c <= 0:     #If Schleife, die prüft, ob die eingegebenen Werte negativ sind. Falls dies zutrifft gibt es eine Warnung
    print("Negativer Quader? das geht nich!")
else:
    print("Volumen:", round(a * b * c, 2))
    print("Oberfläche:", round(2 * (a * b + a * c + b * c), 2))
    print("Raumdiagonale:", round((a * a + b * b + c * c) ** 0.5, 2))