zahlen = []     #leere Liste

for n in range(4):                      #For Schleife, die 4 mal ausgeführt wird
    zahlen.append(float(input("Welche Zahl möchtest du hinzufügen?")))  #Input, der die Zahlen zur Liste hinzufügt

print("Mittelwert:", sum(zahlen) / 4)   #Anstatt der 4 kann auch die Funktion [len.zahlen] verwendet werden