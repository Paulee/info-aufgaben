a = float(input("Welche Breite?"))  #Maße des Quaders werden als verschiedene Variablen definiert
b = float(input("Welche Höhe?"))
c = float(input("Welche Länge?"))

print("Volumen:", round(a * b * c, 2)   #Rechnungen (siehe Tafelwerk)
print("Oberfläche:", round(2 * (a * b + a * c + b * c), 2))
print("Raumdiagonale:", round((a * a + b * b + c * c) ** 0.5, 2))