preis = float(input("was muss er Bezahlen?"))   #Input der zu zahlenden Summe
bar = float(input("Wie viel hat er bezahlt?"))  #Input der gegebenen Summe

if preis < bar:                                 #Falls der Preis kleiner als das gegebene Geld ist, wird das Rückgeld berechnet
    print(round( bar - preis , 2), "€ ist das Rückgeld")    #Ausgabe
elif preis == bar:                              #Falls alles übereinstimmt, wird einfach ein Danke ausgegeben
    print("Danke für Ihren Einkauf")
else:
    print("Da fehlen noch",round( preis - bar , 2), "€")                   #Ausgabe, die Erfolgt, wenn das gegebene Geld nicht größer als der Preis ist