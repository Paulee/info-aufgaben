n = int(input("Zahl?"))

expo = 1

while (2 ** (expo + 1)) <= n:
    expo += 1

print(2 ** expo)