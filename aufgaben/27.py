n = int(input("Wie viele möchtest du anzeigen"))

def fibonacci(i):
    if i == 0:
        return 0
    elif i == 1:
        return 1
    else:
        return fibonacci(i - 1) + fibonacci(i - 2)

x = 0

while x < n:
    print(fibonacci(x))
    x += 1

