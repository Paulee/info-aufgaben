größe = float(input("Größe in m?"))         #Funktioniert wie Aufgabe 10. Es werden zwei If Schleifen verknüpft
gewicht = float(input("Gewicht in Kg?"))    #Eingabe der Variablen (a,b,c)
sex = input("Geschlecht?(M/W)")

bmi = round(float(gewicht / (größe * größe)), 2)    #Errechnung BMI

if sex == "w":                              #If Schhleife 1 (W oder M)
    if bmi < 19:
        print("Du hast ein Untergewicht")   #If Schleife 2 (BMI)
    elif bmi > 24:
        print("Du hast ein Übergewicht")    #If Schleife 2 (BMI)
    else:
        print("Du hast ein Normalgewicht")  #If Schleife 2 (BMI)

    print("Dein BMI:", bmi)

elif sex == "m":                           #If Schhleife 1 (W oder M)
    if bmi < 20:
        print("Du hast ein Untergewicht")   #If Schleife 2 (BMI)
    elif bmi > 25:
        print("Du hast ein Übergewicht")    #If Schleife 2 (BMI)
    else:
        print("Du hast ein Normalgewicht")  #If Schleife 2 (BMI)

    print("Dein BMI:", bmi)