#Aufgabe 1
def buchstaben(text):
    print(text[0], text[len(text) - 1])


buchstaben("Schule")

#Aufgabe 2
def summe(max):
    result = 0;
    for i in range(max):
        result += (i + 1)
    return result


print(summe(100))

#Aufgabe 3
def teilermenge(n):
    teiler = []
    for i in range(n):
        if n % (i + 1) == 0:
            teiler.append(i + 1)
    return teiler


print(teilermenge(24))