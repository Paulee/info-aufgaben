# Aufgabe 1
def fakultaet(zahl):
    z = 1
    for i in range(zahl):
        z *= (i + 1)
    return z


print("Ihre Fakultät:", fakultaet(int(input("Deine erste Zahl?"))))
print("Ihre Fakultät:", fakultaet(int(input("Deine zweite Zahl?"))))


# Aufgabe 2
def quersumme(zahl):
    z = 0
    for i in str(zahl):
        z += int(i)
    return z


print("Quersumme aus 1337 ist:", quersumme(1337))


# Aufgabe 4
#    Definition 3 ist zulässig, da es keine doppelten Namen der Methode gibt und sie dem Aufbau einer Funktion übereinstimmt.

# Aufgabe 5
def dez_to_bin(dezzahl):
    binary = ""
    while dezzahl != 0:
        binary += str(dezzahl % 2)
        dezzahl //= 2
    return binary[::-1]


def bin_to_dez(binzahl):
    binzahl = str(binzahl)[::-1]
    zahl = 0
    for i in range(len(binzahl)):
        zahl += int(binzahl[i]) * 2 ** i
    return zahl


print(dez_to_bin(24))
print(bin_to_dez(11000))
