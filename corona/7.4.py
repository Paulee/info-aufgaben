# Aufgabe 1
'''
    1. -> geht
    2. -> geht nicht, da a schon definiert ist
    3. -> geht nicht, da ein Parameter fehlt bei der Übergabe
    4. -> geht nicht, da kp
'''


# Aufgabe 2
def add_function(f, g):
    return f(2) + g(2)


print(add_function(lambda x: 2 ** x, lambda x: 3 ** x))
print(add_function(lambda x: 5 ** x, lambda x: 7 ** x))
